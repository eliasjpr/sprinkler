-- +micrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE INDEX CONCURRENTLY IF NOT EXISTS device_id_idx ON devices(device_id);

-- +micrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP INDEX device_id_idx;