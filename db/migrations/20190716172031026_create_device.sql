-- +micrate Up
CREATE TABLE devices (
  id BIGSERIAL PRIMARY KEY,
  device_id VARCHAR,
  topic VARCHAR,
  value FLOAT,
  unit VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);


-- +micrate Down
DROP TABLE IF EXISTS devices;
