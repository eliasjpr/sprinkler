require "granite/adapter/pg"

db_pool_size = ENV["DB_POOL_SIZE"]? || "4"
db_checkout_timeout = ENV["DB_CHECKOUT_TIMEOUT"]? || "0.1"

database_url = Amber.settings.database_url + "?ssl_mode=required&initial_pool_size=#{db_pool_size}&max_pool_size=#{db_pool_size}&checkout_timeout=#{db_checkout_timeout}"
Granite::Adapters << Granite::Adapter::Pg.new({name: "pg", url: database_url})
Granite.settings.logger = Amber.settings.logger.dup
Granite.settings.logger.not_nil!.progname = "Granite"
