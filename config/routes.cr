Amber::Server.configure do
  pipeline :api do
    plug Amber::Pipe::Logger.new if Amber.env.development?
  end

  routes :api do
    get "/devices", DeviceController, :index
    get "/devices/:device_id", DeviceController, :show
    post "/devices", DeviceController, :create
    get "/health", HomeController, :health
  end
end
