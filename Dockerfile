FROM amberframework/amber

RUN apt-get update && apt-get install -y \
    libssl-dev \
    libxml2-dev \
    libyaml-dev \
    libgmp-dev \
    libreadline-dev \
    libz-dev

RUN apt install crystal

ENV APP_ROOT /app
ENV PORT 4000
EXPOSE 4000

RUN mkdir ${APP_ROOT}
RUN mkdir ${APP_ROOT}/bin
WORKDIR ${APP_ROOT}

COPY . ${APP_ROOT}
COPY shard.* ${APP_ROOT}/
RUN shards install 

COPY ./setup.sh /usr/bin/
RUN chmod +x /usr/bin/setup.sh
ENTRYPOINT ["setup.sh"]

CMD ["./bin/sprinkler"]
