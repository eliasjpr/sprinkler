ENV["AMBER_ENV"] ||= "test"

require "spec"
require "../config/application"

# Disable Granite logs in tests
Granite.settings.logger = Amber.settings.logger.dup
