class Device < Granite::Base
  adapter pg
  table_name devices

  primary id : Int64
  field device_id : String
  field topic : String
  field value : Float64
  field unit : String
  timestamps
end
