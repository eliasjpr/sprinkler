require "../requests/*"
require "../responses/*"

class DeviceController < ApplicationController
  getter device = Device.new
  getter empty_json = {} of String => String

  before_action do
    only [:show, :update, :destroy] { set_device }
  end

  def index
    {devices: Device.all}.to_json
  end

  def show
    {device: device}.to_json
  end

  def create
    request = Requests::DeviceRequest.from_json(params[:_json])
    device = request.device
    device.save
    response.status = HTTP::Status::CREATED
    {"device": device}.to_json
  rescue e
    response.status = HTTP::Status::INTERNAL_SERVER_ERROR
    Response::Error.new(e.class.to_s, e.message, nil).to_json
  end

  def update
    device.set_attributes device_params.validate!
    if device.save
      redirect_to action: :index, flash: {"success" => "Updated device successfully."}
    else
      flash[:danger] = "Could not update Device!"
      flash.to_json
    end
  end

  def destroy
    device.destroy
    redirect_to action: :index, flash: {"success" => "Deleted device successfully."}
  end

  private def device_params
    params.validation do
      required :device_id
      required :topic
      required :value
      required :unit
    end
  end

  private def set_device
    device_id = params[:device_id].capitalize
    @device = Device.first! "WHERE device_id = ?", [device_id]
  end
end
