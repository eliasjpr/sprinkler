class HomeController < ApplicationController
  def health
    {
      status:     "pass",
      app_name:   Amber.settings.name,
      version:    "0.1.0",
      cpu_count:  System.cpu_count,
      hostname:   System.hostname,
      client_ip:  context.client_ip.to_s,
      host:       context.request.host,
      port:       Amber.settings.port,
      port_reuse: Amber.settings.port_reuse,
      time:       Time.now,
      uptime:     3.days.ago,
    }.to_json
  end
end
