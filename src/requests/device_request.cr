require "json"
require "../models/device"

module Requests
  struct DeviceRequest
    include JSON::Serializable

    getter metadata : Metadata
    getter device : Device
  end
end
