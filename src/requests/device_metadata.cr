require "json"

module Requests
  struct Metadata
    include JSON::Serializable

    getter sysname : String
    getter nodename : String
    getter release : String
    getter version : String
    getter machine : String
  end
end
