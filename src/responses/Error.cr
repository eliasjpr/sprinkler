require "json"
require "uuid"

module Response
  struct Error
    include JSON::Serializable

    getter status : HTTP::Status = HTTP::Status::BAD_REQUEST
    getter code : Int32 = 0
    getter title : String = "Error occurred"
    getter detail : String?
    getter source : String?

    def initialize(@title, @detail, @source)
    end
  end
end
