#!/bin/sh
set -e

crystal build ./src/sprinkler.cr --release -o ./bin/sprinkler
chmod +x ./bin/sprinkler

exec "$@"
